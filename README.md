# cinephiles

Es un módulo para consultar información sobre las grandes obras del cine contemporáneo y tradicional, la información se obtiene por medio del servicio web proporcionado por [**Omdbapi**](http://www.omdbapi.com/) el cual entrga en su API un servicio gratuito y uno Prémium, pero ambos entregan amplia información sobre películas y series.

El módulo principalmente carga las cintas predeterminadas y el usuario puede consultar cualquier nombre de películas o series mediante la función search proporcionada en el Navbar de la aplicación, pudiendo elegir el tipo de búsqueda que desea realizar.

## VueJS

[**VueJS**](https://vuejs.org/) es uno de los frameworks que representan un mayor uso en el mercado y que han demostrado ser una garantía de robustez y escalabilidad.

El core principal de VueJS está formado por una librería encargada de renderizar vistas en el navegador. Su forma de organizar el código es por medio de pequeños componentes que contienen todo el HTML, CSS y JavaScript necesario para funcionar como pieza independiente.

## Principales librerias utilizadas en el proyecto

- [**Typescript :**](https://www.typescriptlang.org/) Realiza una formalización de los tipos de Javascript, mediante una representación estática de sus tipos dinámicos. Esto permite a los desarrolladores definir variables y funciones tipadas sin perder la esencia de Javascript. Poder definir los tipos durante el tiempo de diseño nos ayuda a evitar errores en tiempo de ejecución, como podría ser pasar el tipo de variable incorrecto a una función.
- [**Vuetify :**](https://vuetifyjs.com/en/)
  Es un framework progresivo de componentes para Vue.js sobre Material Design.
- [**Vuex :**](https://vuex.vuejs.org/) Se trata de una implementación de Flux, un patrón de diseño para controlar el estado de nuestras aplicaciones.
  Vuex simplifica la forma en que nuestros componentes se comunican entre ellos, centralizando este proceso.

- [**Axios :**](https://www.npmjs.com/package/axios) Axios es una librería JavaScript que puede ejecutarse en el navegador y que nos permite hacer sencillas las operaciones como cliente HTTP, por lo que podremos configurar y realizar solicitudes a un servidor y recibiremos respuestas fáciles de procesar
- [**vue-router :**](https://router.vuejs.org/) Es la biblioteca de enrutamiento oficial de Vuejs para el lado del cliente que proporciona las herramientas necesarias para asignar los componentes de una aplicación a diferentes rutas de URL del navegador.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
