import {
  VuexModule,
  Module,
  Mutation,
  Action,
  config,
  getModule,
} from "vuex-module-decorators";
import { Search, SearchMovie } from "../models/searchs";
import axios from "axios";
import store from "@/store";

config.rawError = true;
@Module({
  namespaced: true,
  dynamic: true,
  store,
  name: "search",
})
class SearchStore extends VuexModule {
  dataSearch: Search[] = [];
  dataMovie: SearchMovie[] = [];

  get dataSearchs() {
    return this.dataSearch;
  }
  get dataMovies() {
    return this.dataMovie;
  }
  @Mutation
  setDataSearch(searchParam: Search[]) {
    this.dataSearch = searchParam;
  }
  @Mutation
  setDataMovies(movieParam: SearchMovie[]) {
    this.dataMovie = movieParam;
  }

  @Action({ commit: "setDataSearch" })
  async getDataSearch(data: { query: string; pages: number }) {
    const response = await axios.get(
      process.env.VUE_APP_API_URL +
        process.env.VUE_APP_API_KEY +
        process.env.VUE_APP_API_PLOT +
        data.query +
        `&page=${data.pages}`
    );
    if (response.data.Response) {
      response.data.totalResults = Math.round(response.data.totalResults / 10);
      const res: Search = await response.data;
      res.payload = data.query;
      return res;
    } else {
      console.error(response);
      return null;
    }
  }

  @Action({ commit: "setDataMovies" })
  async getDataMovies(query: string) {
    const response = await axios.get(
      process.env.VUE_APP_API_URL +
        process.env.VUE_APP_API_KEY +
        process.env.VUE_APP_API_PLOT +
        query
    );
    if (response.data.Response) {
      const res: Search = await response.data;
      return res;
    } else {
      console.error(response);
      return null;
    }
  }
}

export default getModule(SearchStore);
