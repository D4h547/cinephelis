import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: "#2b8df2",
        secondary: "#515c6f",
        accent: "#82B1FF",
        error: "#E57373",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107",
        background: "#e0e0e0",
      },
    },
  },
});
